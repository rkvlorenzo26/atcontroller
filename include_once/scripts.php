<script src="resources/jquery-1.10.2.js"></script>
<script type="text/javascript">
$(document).ready(function($){
  loadSettings();

  setInterval(function(){
    getSensorReadings();
  },1000);
});

function getSensorReadings() {
  $.ajax({
    url: 'functions/get_sensor_readings.php',
    type: 'GET',
    success: function(response) {
        var apiResponse = JSON.parse(response);
        if (apiResponse.result == "Success") {
          var date = apiResponse.date;
          var temperature = apiResponse.temperature;
          var humidity = apiResponse.humidity;
          $('#lastUpdate').text("Updated as of: " + date);
          $('#temperature').text(temperature);
          $('#humidity').text(humidity)
        }
    }
  });
}

function loadSettings() {
  $("[type='number']").keypress(function (evt) {
    evt.preventDefault();
  });

  $.ajax({
    url: 'functions/get_mode_settings.php',
    type: 'GET',
    success: function(response) {
        var apiResponse = JSON.parse(response);
        if (apiResponse.result == "Success") {
          var status = apiResponse.status;
          var temperature = apiResponse.temperature;
          $('#adaptive_settings').text(status.toUpperCase());
          $('#temperature_settings').val(temperature);
          
          if (status === "ON") {
            $('#adaptive_text').text("MANUAL");
            $("#btn_temp_up").attr("disabled", true);
            $("#btn_temp_down").attr("disabled", true);
            $("#btn_power").attr("disabled", true);
            $("#btn_save_temp").removeAttr("disabled");
            $("#temperature_settings").removeAttr("disabled");
          } else if (status === "OFF") {
            $('#adaptive_text').text("AUTOMATIC");
            $("#btn_temp_up").removeAttr("disabled");
            $("#btn_temp_down").removeAttr("disabled");
            $("#btn_power").removeAttr("disabled");
            $("#btn_save_temp").attr("disabled", true);
            $("#temperature_settings").attr("disabled", true);
          }
        }
    }
  });
}

function adaptiveSettings() {
  $("#btn_mode").attr("disabled", true);
  var value = $('#adaptive_settings').text();
  if (value === "OFF") {
    var data = "ON";
    sendCommand('ADAPTIVE', data);
    updateAdaptiveSettings(data);
  } else if (value === "ON") {
    var data = "OFF";
    sendCommand('ADAPTIVE', data);
    updateAdaptiveSettings(data)
  }
  $("#btn_mode").removeAttr("disabled");
}

function updateAdaptiveSettings(value) {
  var data = {
      "value" : value
  }
  $.ajax({
    url: 'functions/update_mode_settings.php',
    type: 'POST',
    data: data,
    success: function(response) {
        var apiResponse = JSON.parse(response);
        if (apiResponse.result == "Success") {
          loadSettings();
        }
    }
  });
}

function saveTemperature() {
  var value = $('#temperature_settings').val();
  var data = {
      "value" : value
  }
  $.ajax({
    url: 'functions/update_temperature_settings.php',
    type: 'POST',
    data: data,
    success: function(response) {
        var apiResponse = JSON.parse(response);
        if (apiResponse.result == "Success") {
          loadSettings();
        }
    }
  });
}

function sendCommand(command, value) {
  var data = {
      "command" : command,
      "value" : value
  }
  $.ajax({
    url: 'functions/save_command.php',
    type: 'POST',
    data: data,
    success: function(response) {
        var apiResponse = JSON.parse(response);
        if (apiResponse.result == "Success") {
          loadSettings();
        }
    }
  });
}
</script>


<script src="resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="resources/bootstrap/js/bootstrap.min.js"></script>
<script src="resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="resources/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="resources/plugins/fastclick/fastclick.js"></script>
<script src="resources/dist/js/app.min.js"></script>
<script src="resources/dist/js/pages/dashboard.js"></script>
<script src="resources/dist/js/demo.js"></script>
