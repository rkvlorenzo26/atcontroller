<header class="main-header">
  <a href="index.html" class="logo">
    <span class="logo-mini"><b>AT</b>C</span>
    <span class="logo-lg"><b>AT</b>Controller</span>
  </a>
  <nav class="navbar navbar-static-top" role="navigation">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
  </nav>
</header>
<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="active"><a href="index.html"><i class="fa fa-home"></i> <span>Home</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>