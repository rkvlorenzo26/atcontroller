<!DOCTYPE html>
<html>
<?php include_once 'include_once/header.php'; ?>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
  <?php include_once 'include_once/sides.php'; ?>
  <div class="content-wrapper">
    <section class="content">
      <div class="row">
        <div class="col-lg-12 col-md-4">
          <h4 id="lastUpdate">Updated as of:  </h4>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="temperature">-</h3>
              <p>Room Temperature</p>
            </div>
            <div class="icon">
              <i class="ion ion-thermometer"></i>
            </div>
            <div class="small-box-footer"></div>
            <div class="small-box-footer"></div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="humidity">-</h3>
              <p>Humidity</p>
            </div>
            <div class="icon">
              <i class="ion ion-cloud"></i>
            </div>
            <div class="small-box-footer"></div>
            <div class="small-box-footer"></div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="adaptive_settings">-</h3>
              <p>Adaptive Setting</p>
            </div>
            <div class="icon">
              <i class="ion ion-settings"></i>
            </div>
            <div class="small-box-footer"></div>
            <div class="small-box-footer"></div>
            <div class="small-box-footer"></div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 col-xs-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Temperature Settings - Automatic Mode</h3>
            </div>
            <div class="box-body" style="height: 150px">
                <div class="input-group">
                  <input type="Number" id="temperature_settings" class="form-control" min="16" max="32" value="16" placeholder="Temperature">
                  <span class="input-group-addon">°</span>
                </div>
                <br>
                <button id="btn_save_temp" href="javascript:void(0)" onclick="saveTemperature()" class="btn btn-block btn-primary">Set Temperature</button>
            </div>
          </div>
        </div>

        <div class="col-lg-6 col-xs-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Controls</h3>
            </div>
            <div class="box-body" style="height: 100%">
              <center>  
                <button id="btn_mode" class="btn btn-app" href="javascript:void(0)" onclick="adaptiveSettings()">
                    <i class="fa fa-play"></i> 
                    <p id="adaptive_text">AUTOMATIC</p>
                </button>
                <button id="btn_power" class="btn btn-app" href="javascript:void(0)" onclick="sendCommand('POWER', 'ON/OFF')">
                  <i class="fa fa-power-off"></i> 
                  <p>ON / OFF</p>
                </button>
                <button id="btn_temp_up" class="btn btn-app" href="javascript:void(0)" onclick="sendCommand('TEMPERATURE', 'UP')">
                    <i class="fa fa-arrow-up"></i> 
                    <p>TEMP. UP</p>
                </button>
                <button id="btn_temp_down" class="btn btn-app" href="javascript:void(0)" onclick="sendCommand('TEMPERATURE', 'DOWN')">
                    <i class="fa fa-arrow-down"></i> 
                    <p>TEMP. DOWN</p>
                </button>
              </center>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="index.html">ATController</a>.</strong> All rights
    reserved.
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<?php include_once 'include_once/scripts.php'; ?>
</body>
</html>
