import broadlink
import Adafruit_DHT
import requests
import logging
import json
import os
import time
import sys

GET_COMMANDS_URL = "http://atcontroller.000webhostapp.com/get_commands.php"
GET_ADAPTIVE_COMMAND_URL = "http://atcontroller.000webhostapp.com/get_adaptive_command.php"
SENSOR_URL = "http://atcontroller.000webhostapp.com/save_sensor_readings.php"
current_temperature = 16
current_humidity = 75

#Get available devices
def getDeviceCommand():
	command = "NONE"
	devices = broadlink.discover(timeout=5)
	for device in devices:
		if device.auth():
			command = "./broadlink_cli --type {} --host {} --mac {}".format(hex(device.devtype), device.host[0],''.join(format(x, '02x') for x in device.mac))
	return command

#Test if command is working
#Temperature of BROADLINK
def testCommand(command):
	print("TESTING COMMAND..")
	checkTemperature = " --temperature"
	os.system(command + checkTemperature)

#Start all process
def startProcess(command):
	print("START PROCESS...")
	start = True
	while(start):
		humidity, temperature = Adafruit_DHT.read_retry(11, 4)
		current_temperature = int(temperature)
		current_humidity = int(humidity)
		parameters = "?temperature=" + str(current_temperature) + "&humidity=" + str(current_humidity)
		request = SENSOR_URL + parameters
		print('Sending sensor values to web: ' + request)
		response = requests.get(url = request)

		#If adaptive is not equal to ON; Check if there are commands from web;
		if os.environ.get('ADAPTIVE', 'NONE') != "ON":
			print("ADAPTIVE CONTROLLER: OFF")
			request = GET_COMMANDS_URL
			print('Sending requests: ' + request)
			response = requests.get(url = request)
			result = response.json()
			executeCommands(result, command)

		#If adaptive is equal to ON;
		#temperature = value from web settings;
		else:
			print("ADAPTIVE CONTROLLER: ON")
			saved_temperature = int(os.environ.get('TEMPERATURE', '18'))
			adaptiveController(command, current_temperature, saved_temperature)

#Reset temperature of AC to lowest possible temperature
def resetACTemperature(broadlink_command):
	print("Reset AC to lowest possible temperature...")
	highest_ac_temp = 32	#Possible highest temperature that is currently set
	lowest_ac_temp = 16	#Possible lowest temperature that is currently set
	counter = highest_ac_temp - lowest_ac_temp
	send = " --send @commands/TEMPERATURE_DOWN"
	while counter > 0:
		os.system(broadlink_command + send)
		counter = counter - 1

#Execute all latest commands from web;
def executeCommands(result, broadlink_command):
	print("Command count: " + str(result['count']))
	send = " --send @commands/"
	if result['count'] > 0:
		commands = result['commands']
		for data in commands:
			#If command is equal to TEMPERATURE; VALUE = UP OR DOWN;
			#Execute TEMPERATURE command; Ex. TEMPERATURE_UP OR TEMPERATURE_DOWN
			if data['command'] == 'TEMPERATURE':
				command = data['command'] + "_" + data['value']
				print("Execute command: " + broadlink_command + send + command)
				os.system(broadlink_command + send + command)

			#If command is equal to POWER;
			#Execute POWER command; Ex. POWER
			if data['command'] == 'POWER':
				command = data['command']
				print("Execute command: " + broadlink_command + send + command)
				os.system(broadlink_command + send + command)

			#If command is equal to ADAPTIVE; VALUE = ON OR OFF;
			if data['command'] == 'ADAPTIVE':
				print(data['command'] + " : " + data['value'])
				#If value is equal to ON; ADAPTIVE = 'ON'; TEMPERATURE = Value from web settings;
				if data['value'] == 'ON':
					os.environ['ADAPTIVE'] = data['value']
					os.environ['TEMPERATURE'] = data['temperature']
				else:
					os.environ['ADAPTIVE'] = data['value']

#Adaptive controller
#saved_temperature = saved temperature from web;
def adaptiveController(broadlink_command, current_temperature, saved_temperature):
	print("ADAPTIVE CONTROLLER...")
	print("Current temperature: " + str(current_temperature))
	print("Saved temperature: " + str(saved_temperature))

	#if current_temperature from sensor is greater than saved_temperature;
	#Reset AC to lowest possible temperature;
	if current_temperature > saved_temperature:
		resetACTemperature(command)

	#if current_temperature from sensor is less than saved_temperature;
	#Execute temperature up command;
	#counter is equal to the number of command that will be executed
	elif current_temperature < saved_temperature:
		counter = saved_temperature - current_temperature
		send = " --send @commands/TEMPERATURE_UP"
		while counter > 0:
			os.system(broadlink_command + send)
			print("TEMPERATURE_UP: " + str(counter))
			counter = counter - 1
	
	#SLEEP FOR 1 MINUTE; To check if there will be changes in temperature;
	#This process will not be interrupted; Commands will be executed after sleep;
	time.sleep(60)
	#Request again if there is a latest adaptive command from web;
	request = GET_ADAPTIVE_COMMAND_URL
	print('Sending requests: ' + request)
	response = requests.get(url = request)
	result = response.json()
	executeCommands(result, command)

#START OF ATCONTROLLER
print("START ATCONTROLLER...")
command = getDeviceCommand()
if command != "NONE":
	print("DEVICE COMMAND: " + command)
	testCommand(command)
	startProcess(command)
else: 
	print("NO DEVICE FOUND...")
	