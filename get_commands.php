<?php		
include_once 'functions/connection.php';
$data = new stdClass();

$time = strtotime($date_now);
$time = $time - 60;
$from_date = date("Y-m-d H:i:s", $time);

//CHECK IF THERE ARE PENDING COMMANDS; CURRENT TIME - 1 MINUTE;
$get_commands = mysqli_query($con, "SELECT * FROM commands WHERE status = 'NEW' AND date >= '$from_date' AND date <= '$date_now'");
$count = mysqli_num_rows($get_commands);

//IF PENDING COMMANDS > 0; READY IT;
if ($count > 0) {
    $data->count = $count;
    $commands = array();
    while ($row = mysqli_fetch_array($get_commands)) {
        $command = new stdClass();
        $command->command = $row['command'];    //COMMAND Ex. POWER, TEMPERATURE, ADAPTIVE
        $command->value = $row['value'];        //VALUE Ex. TEMPERATURE: UP/DOWN, ADAPTIVE: ON/OFF, POWER: ON/OFF

        //IF ADAPTIVE IS ON; INCLUDE TEMPERATURE SETTINGS VALUE;
        if ($row['command'] == "ADAPTIVE" && $row['value'] == "ON") {
            $get_temperature = mysqli_query($con, "SELECT * FROM settings WHERE name = 'temperature'");
            $temp = mysqli_fetch_array($get_temperature); 
            $command->temperature = $temp['value'];
        }

        //UPDATE PENDING COMMAND TO SENT
        $id = $row['id'];
        $update = mysqli_query($con, "UPDATE commands SET status ='SENT' WHERE id = '$id'");
        array_push($commands, $command);
    }
    $data->commands = $commands;
} else {
    $data->count = $count;
}

$myJSON = json_encode($data);
echo $myJSON;
?>