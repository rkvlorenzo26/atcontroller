<?php		
include_once 'connection.php';
$data = new stdClass();

//GET MODE; ON = AUTOMATIC; OFF = MANUAL;
$get_mode = mysqli_query($con, "SELECT * FROM settings WHERE name = 'mode'");
//GET TEMPERATURE SETTINGS;
$get_temperature = mysqli_query($con, "SELECT * FROM settings WHERE name = 'temperature'");

if (mysqli_num_rows($get_mode) > 0) {
    //READY DATA
    $mode = mysqli_fetch_array($get_mode); 
    $data->result = "Success";
    $data->status = $mode['value'];

    $temp = mysqli_fetch_array($get_temperature); 
    $data->temperature = $temp['value'];
} else {
    $data->result = "Invalid";
}

$myJSON = json_encode($data);
echo $myJSON;
?>