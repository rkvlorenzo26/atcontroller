<?php		
include_once 'connection.php';
$data = new stdClass();

//GET LATEST TEMPERATURE FROM RPI;
$get_readings = mysqli_query($con, "SELECT * FROM readings ORDER BY date DESC limit 1");

if (mysqli_num_rows($get_readings) > 0) {
    //READY DATA;
    $row = mysqli_fetch_array($get_readings); 
    $data->result = "Success";
    $data->temperature = $row['temperature'];
    $data->humidity = $row['humidity'];
    $data->date = $row['date'];
} else {
    $data->result = "Invalid";
}

$myJSON = json_encode($data);
echo $myJSON;
?>